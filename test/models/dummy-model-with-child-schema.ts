import Model from '../../src/model';
import DummyModel from './dummy-model.nongo';

export class DummyModelWithChildSchema extends Model {
  public defineSchema(): any {
    return {
      dummy: {
        type: DummyModel,
      },
    };
  }
}
