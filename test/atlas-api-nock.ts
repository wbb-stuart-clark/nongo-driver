// tslint:disable:max-line-length
import nock from 'nock';

export const setUpInterceptors = () => {

    const basePath = 'https://cloud.mongodb.com:443';
    const options = {encodedQueryParams: true};
    nock(basePath, options)
    .get('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/monmouth-county-council/Entity')
    .reply(401, {reason: 'Unauthorized', error: 401, detail: 'You are not authorized for this resource.'}, [ 'www-authenticate',
      'Digest realm="MMS Public API", domain="", nonce="UExrY6eX+h+3rGTYa4OX8v078P6Cra1e", algorithm=MD5, qop="auth", stale=false',
      'content-type',
      'application/json',
      'content-length',
      '106',
      'x-envoy-upstream-service-time',
      '3',
      'date',
      'Tue, 06 Apr 2021 12:28:28 GMT',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .get('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/monmouth-county-council/Entity')
    .reply(200, [], [ 'date',
      'Tue, 06 Apr 2021 12:28:28 GMT',
      'x-mongodb-service-version',
      'gitHash=5e631a8df30283a365cfd23f5d8b9b2cd98fd598; versionString=v20210330',
      'content-type',
      'application/json',
      'strict-transport-security',
      'max-age=31536000',
      'x-frame-options',
      'DENY',
      'vary',
      'Accept-Encoding, User-Agent',
      'content-length',
      '2',
      'x-envoy-upstream-service-time',
      '20',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .post('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes', {collectionName: 'Entity', database: 'monmouth-county-council', mappings: {dynamic: true}, name: 'index1'})
    .reply(401, {reason: 'Unauthorized', error: 401, detail: 'You are not authorized for this resource.'}, [ 'www-authenticate',
      'Digest realm="MMS Public API", domain="", nonce="AlZ8/F9pIp/mSSSNowUA3s6LGG/YwBJN", algorithm=MD5, qop="auth", stale=false',
      'content-type',
      'application/json',
      'content-length',
      '106',
      'x-envoy-upstream-service-time',
      '1',
      'date',
      'Tue, 06 Apr 2021 12:28:28 GMT',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .post('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes', {collectionName: 'Entity', database: 'monmouth-county-council', mappings: {dynamic: true}, name: 'index1'})
    .reply(200, {collectionName: 'Entity', database: 'monmouth-county-council', indexID: '606c53edf81dfe4a33b5caeb', mappings: {dynamic: true}, name: 'index1', status: 'IN_PROGRESS'}, [ 'date',
      'Tue, 06 Apr 2021 12:28:29 GMT',
      'x-mongodb-service-version',
      'gitHash=5e631a8df30283a365cfd23f5d8b9b2cd98fd598; versionString=v20210330',
      'content-type',
      'application/json',
      'strict-transport-security',
      'max-age=31536000',
      'x-frame-options',
      'DENY',
      'content-length',
      '168',
      'x-envoy-upstream-service-time',
      '214',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .post('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes', {collectionName: 'Entity', database: 'monmouth-county-council', mappings: {dynamic: true}, name: 'index2'})
    .reply(401, {reason: 'Unauthorized', error: 401, detail: 'You are not authorized for this resource.'}, [ 'www-authenticate',
      'Digest realm="MMS Public API", domain="", nonce="TqPzNAanepISeZLLzWtQs2ap5on/3mo0", algorithm=MD5, qop="auth", stale=false',
      'content-type',
      'application/json',
      'content-length',
      '106',
      'x-envoy-upstream-service-time',
      '1',
      'date',
      'Tue, 06 Apr 2021 12:28:29 GMT',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .post('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes', {collectionName: 'Entity', database: 'monmouth-county-council', mappings: {dynamic: true}, name: 'index2'})
    .reply(200, {collectionName: 'Entity', database: 'monmouth-county-council', indexID: '606c53ed4012d1669a8bbeed', mappings: {dynamic: true}, name: 'index2', status: 'IN_PROGRESS'}, [ 'date',
      'Tue, 06 Apr 2021 12:28:29 GMT',
      'x-mongodb-service-version',
      'gitHash=5e631a8df30283a365cfd23f5d8b9b2cd98fd598; versionString=v20210330',
      'content-type',
      'application/json',
      'x-frame-options',
      'DENY',
      'strict-transport-security',
      'max-age=31536000',
      'content-length',
      '168',
      'x-envoy-upstream-service-time',
      '44',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .get('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/monmouth-county-council/Entity')
    .reply(401, {reason: 'Unauthorized', error: 401, detail: 'You are not authorized for this resource.'}, [ 'www-authenticate',
      'Digest realm="MMS Public API", domain="", nonce="PU9J4MCr46nXNmhJ3iAk2rWFQ9ib8Lvc", algorithm=MD5, qop="auth", stale=false',
      'content-type',
      'application/json',
      'content-length',
      '106',
      'x-envoy-upstream-service-time',
      '1',
      'date',
      'Tue, 06 Apr 2021 12:28:30 GMT',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .get('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/monmouth-county-council/Entity')
    .reply(200, [{collectionName: 'Entity', database: 'monmouth-county-council', indexID: '606c53edf81dfe4a33b5caeb', mappings: {dynamic: true}, name: 'index1', status: 'IN_PROGRESS'}, {collectionName: 'Entity', database: 'monmouth-county-council', indexID: '606c53ed4012d1669a8bbeed', mappings: {dynamic: true}, name: 'index2', status: 'IN_PROGRESS'}], [ 'date',
      'Tue, 06 Apr 2021 12:28:30 GMT',
      'x-mongodb-service-version',
      'gitHash=5e631a8df30283a365cfd23f5d8b9b2cd98fd598; versionString=v20210330',
      'content-type',
      'application/json',
      'x-frame-options',
      'DENY',
      'strict-transport-security',
      'max-age=31536000',
      'vary',
      'Accept-Encoding, User-Agent',
      'content-length',
      '339',
      'x-envoy-upstream-service-time',
      '22',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .get('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/monmouth-county-council/Entity')
    .reply(401, {reason: 'Unauthorized', error: 401, detail: 'You are not authorized for this resource.'}, [ 'www-authenticate',
      'Digest realm="MMS Public API", domain="", nonce="bY2x8zJ76stKh4XHMtsjWVtEM+YSWNrj", algorithm=MD5, qop="auth", stale=false',
      'content-type',
      'application/json',
      'content-length',
      '106',
      'x-envoy-upstream-service-time',
      '2',
      'date',
      'Tue, 06 Apr 2021 12:28:30 GMT',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .get('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/monmouth-county-council/Entity')
    .reply(200, [{collectionName: 'Entity', database: 'monmouth-county-council', indexID: '606c53edf81dfe4a33b5caeb', mappings: {dynamic: true}, name: 'index1', status: 'IN_PROGRESS'}, {collectionName: 'Entity', database: 'monmouth-county-council', indexID: '606c53ed4012d1669a8bbeed', mappings: {dynamic: true}, name: 'index2', status: 'IN_PROGRESS'}], [ 'date',
      'Tue, 06 Apr 2021 12:28:31 GMT',
      'x-mongodb-service-version',
      'gitHash=5e631a8df30283a365cfd23f5d8b9b2cd98fd598; versionString=v20210330',
      'content-type',
      'application/json',
      'strict-transport-security',
      'max-age=31536000',
      'x-frame-options',
      'DENY',
      'vary',
      'Accept-Encoding, User-Agent',
      'content-length',
      '339',
      'x-envoy-upstream-service-time',
      '26',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .patch('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/606c53ed4012d1669a8bbeed', {collectionName: 'Entity', database: 'monmouth-county-council', mappings: {dynamic: false}, name: 'index2'})
    .reply(401, '', [ 'www-authenticate',
      'Digest realm="MMS Public API", domain="", nonce="njJpLwlEeLvQf3dOilxXUB77H9wDXT24", algorithm=MD5, qop="auth", stale=false',
      'content-length',
      '0',
      'x-envoy-upstream-service-time',
      '2',
      'date',
      'Tue, 06 Apr 2021 12:28:31 GMT',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .patch('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/606c53ed4012d1669a8bbeed', {collectionName: 'Entity', database: 'monmouth-county-council', mappings: {dynamic: false}, name: 'index2'})
    .reply(200, {collectionName: 'Entity', database: 'monmouth-county-council', indexID: '606c53ed4012d1669a8bbeed', mappings: {dynamic: false}, name: 'index2', status: 'IN_PROGRESS'}, [ 'date',
      'Tue, 06 Apr 2021 12:28:31 GMT',
      'x-mongodb-service-version',
      'gitHash=5e631a8df30283a365cfd23f5d8b9b2cd98fd598; versionString=v20210330',
      'content-type',
      'application/json',
      'strict-transport-security',
      'max-age=31536000',
      'x-frame-options',
      'DENY',
      'content-length',
      '169',
      'x-envoy-upstream-service-time',
      '38',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .delete('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/606c53edf81dfe4a33b5caeb')
    .reply(401, '', [ 'www-authenticate',
      'Digest realm="MMS Public API", domain="", nonce="wBtGB1NtXe+dORI0JHSGHYirS5pkwOks", algorithm=MD5, qop="auth", stale=false',
      'content-length',
      '0',
      'x-envoy-upstream-service-time',
      '2',
      'date',
      'Tue, 06 Apr 2021 12:28:32 GMT',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .delete('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/606c53edf81dfe4a33b5caeb')
    .reply(202, {}, [ 'date',
      'Tue, 06 Apr 2021 12:28:32 GMT',
      'x-mongodb-service-version',
      'gitHash=5e631a8df30283a365cfd23f5d8b9b2cd98fd598; versionString=v20210330',
      'content-type',
      'application/json',
      'x-frame-options',
      'DENY',
      'strict-transport-security',
      'max-age=31536000',
      'content-length',
      '2',
      'x-envoy-upstream-service-time',
      '39',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .get('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/monmouth-county-council/Entity')
    .reply(401, {reason: 'Unauthorized', error: 401, detail: 'You are not authorized for this resource.'}, [ 'www-authenticate',
      'Digest realm="MMS Public API", domain="", nonce="Md2CsmM16eaPnT9CJtElaOZPMKZRKdcj", algorithm=MD5, qop="auth", stale=false',
      'content-type',
      'application/json',
      'content-length',
      '106',
      'x-envoy-upstream-service-time',
      '2',
      'date',
      'Tue, 06 Apr 2021 12:28:32 GMT',
      'server',
      'envoy',
      'connection',
      'close' ]);

    nock(basePath, options)
    .get('/api/atlas/v1.0/groups/5b06b6b34e658110696b1da3/clusters/wbb-stage/fts/indexes/monmouth-county-council/Entity')
    .reply(200, [{collectionName: 'Entity', database: 'monmouth-county-council', indexID: '606c53edf81dfe4a33b5caeb', mappings: {dynamic: true}, name: 'index1', status: 'IN_PROGRESS'}, {collectionName: 'Entity', database: 'monmouth-county-council', indexID: '606c53ed4012d1669a8bbeed', mappings: {dynamic: false}, name: 'index2', status: 'IN_PROGRESS'}], [ 'date',
      'Tue, 06 Apr 2021 12:28:32 GMT',
      'x-mongodb-service-version',
      'gitHash=5e631a8df30283a365cfd23f5d8b9b2cd98fd598; versionString=v20210330',
      'content-type',
      'application/json',
      'strict-transport-security',
      'max-age=31536000',
      'x-frame-options',
      'DENY',
      'vary',
      'Accept-Encoding, User-Agent',
      'content-length',
      '340',
      'x-envoy-upstream-service-time',
      '24',
      'server',
      'envoy',
      'connection',
      'close' ]);
};
