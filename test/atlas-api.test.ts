import nock from 'nock';
import AtlasApi from '../src/atlas-api';
import {AtlasParams, AtlasSearchIndex, CreateSearchIndex} from '../src/interface/atlas-search';
import {setUpInterceptors} from './atlas-api-nock';

const groupId = '5b06b6b34e658110696b1da3';
const clusterName = 'wbb-stage';
const collectionName = 'Entity';
const database = 'monmouth-county-council';

const params: AtlasParams = {
  groupId,
  baseUrl: 'https://cloud.mongodb.com/api/atlas/v1.0',
  publicKey: 'aPublicKey',
  privateKey: 'aPrivateKey',
  clusterName,
};

it('Search index management', async () => {

  setUpInterceptors();

  const createIndex = (name: string): CreateSearchIndex => ({
    mappings: {dynamic: true},
    name,
  });

  const index1 = createIndex('index1');
  const index2 = createIndex('index2');
  const api = new AtlasApi(params, database, collectionName);

  const firstEnsure = await api.ensureSearchIndexes([index1, index2]);

  const firstExpected: AtlasSearchIndex[] = [
    {
      collectionName,
      database,
      indexID: '606c53edf81dfe4a33b5caeb',
      mappings: {dynamic: true},
      name: 'index1',
      status: 'IN_PROGRESS',
    },
    {
      collectionName,
      database,
      indexID: '606c53ed4012d1669a8bbeed',
      mappings: {dynamic: true},
      name: 'index2',
      status: 'IN_PROGRESS',
    },
  ];
  expect(firstEnsure).toStrictEqual(firstExpected);

  index2.mappings.dynamic = false;

  // This endpoint takes a moment to update so not testing the response here
  await api.ensureSearchIndexes([index2]);

  // Instead just make sure all the interceptors were used
  expect(nock.isDone());

});
