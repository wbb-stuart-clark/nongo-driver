export * from './atlas-search';

/**
 * Defines an object that can be created via the 'new' keyword
 */
export type Newable<T = any> = new(...args: any[]) => T;

export interface NongoWriteResult {
  nModified: number;
  ok: number;
  n: number;
}

export interface NongoDeleteResult {
  ok?: number;
  n?: number;
}
