export interface Mappings {
  dynamic: boolean;
}

export interface AtlasSearchIndex {
  collectionName: string;
  database: string;
  indexID: string;
  mappings: Mappings;
  name: string;
  status: string;
}

export interface CreateSearchIndex {
  mappings: Mappings;
  name: string;
}

export interface AtlasParams {
  baseUrl: string;
  groupId: string;
  clusterName: string;
  publicKey: string;
  privateKey: string;
  disabled?: boolean;
}
