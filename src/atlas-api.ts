import deepEqual from 'deep-equal';
import rp from 'request-promise';
import Logger from 'wbb-logger';
import {AtlasParams, AtlasSearchIndex, CreateSearchIndex} from './interface/atlas-search';

const logger = new Logger('atlas-api.ts');

export default class AtlasApi {

  private readonly ftsUrl: string;
  private readonly defaultOptions;

  constructor(private params: AtlasParams, private db: string, private collection: string) {
    const {baseUrl, groupId, clusterName, publicKey, privateKey} = params;

    this.defaultOptions = {
      auth: {
        user: publicKey,
        pass: privateKey,
        sendImmediately: false,
      },
      json: true,
    };

    this.ftsUrl = `${baseUrl}/groups/${groupId}/clusters/${clusterName}/fts`;
  }

  public async getSearchIndexes(): Promise<AtlasSearchIndex[]> {
    const url = `${this.ftsUrl}/indexes/${this.db}/${this.collection}`;
    return rp({
      ...this.defaultOptions,
      url,
    });
  }

  public async createSearchIndex(index: CreateSearchIndex): Promise<any> {
    this.logIndexChange('Creating', index.name);
    const url = `${this.ftsUrl}/indexes`;
    return rp({
      ...this.defaultOptions,
      method: 'POST',
      url,
      body: {
        collectionName: this.collection,
        database: this.db,
        ...index,
      },
    });
  }

  public async ensureSearchIndexes(createParams: CreateSearchIndex[]): Promise<AtlasSearchIndex[]> {
    const existingIndexes = await this.getSearchIndexes();
    const toRemove = new Map<string, AtlasSearchIndex>();
    existingIndexes.forEach((e) => toRemove.set(e.name, e));

    for (const cParams of createParams) {
      const {name} = cParams;
      const existing = toRemove.get(name);

      if (existing) {
        toRemove.delete(name);
        await this.updateSearchIndex(existing, cParams);
      } else {
        await this.createSearchIndex(cParams);
      }

    }

    for (const index of toRemove.values()) {
      await this.deleteSearchIndex(index);
    }

    return await this.getSearchIndexes();
  }

  public async updateSearchIndex(existingIndex: AtlasSearchIndex, createParams: CreateSearchIndex): Promise<any> {
    if (deepEqual(existingIndex.mappings, createParams.mappings)) {
      return;
    }

    this.logIndexChange('Updating', createParams.name);

    const url = `${this.ftsUrl}/indexes/${existingIndex.indexID}`;

    return rp({
      ...this.defaultOptions,
      method: 'PATCH',
      url,
      body: {
        collectionName: this.collection,
        database: this.db,
        ...createParams,
      },
    });
  }

  public async deleteSearchIndex(existingIndex: AtlasSearchIndex): Promise<any> {
    this.logIndexChange('Deleting', existingIndex.name);
    const url = `${this.ftsUrl}/indexes/${existingIndex.indexID}`;
    return rp({
      ...this.defaultOptions,
      method: 'DELETE',
      url,
    });
  }

  private logIndexChange(action: string, indexName: string) {
    logger.info(`${action} search index "${indexName}" for collection ${this.db}.${this.collection}`);
  }

}
